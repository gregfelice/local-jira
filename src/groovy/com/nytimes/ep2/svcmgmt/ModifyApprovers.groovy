package com.nytimes.ep2.svcmgmt

import com.nytimes.ep2.utils.Utils

/*
  
  6 approver types

  email them.
  
  thats it. 
  
*/

def u = new Utils()

def log = u.getLog('com.nytimes.ep2.svcmgmt.ModifyApprovers')

def developmentApprover = u.getCustomField(issue, "Development Approver")

// def reviewTasks = u.getSubtasksOfType(issue, 'Review task')

// close all review tasks
// reviewTasks.forEach { task ->
//}

// transition back.
/*
//Map pms = [:]
//pms['transientVars'] = transientVars
//pms['user'] = u.getCurrentUser(transientVars)
//u.transitionIssue(issue, "721", pms) // from modify approvers, resume progress
*/

def project = issue.getProjectObject()
def assignee = u.getUser('greg.felice')
def reporter = u.getUser('greg.felice')
def params = [
  parentIssue: issue,
  issueType: '10', // review task
  project: project,
  assignee: assignee,
  reporter: reporter,
  summary: "Development Approval SubTask"
]
def issue = u.createIssue(params)




