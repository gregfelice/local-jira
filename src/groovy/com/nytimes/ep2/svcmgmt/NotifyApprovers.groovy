package com.nytimes.ep2.svcmgmt

import com.atlassian.crowd.embedded.api.User
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.ManagerFactory
import com.atlassian.jira.config.properties.APKeys
import com.atlassian.jira.config.properties.ApplicationProperties
import com.atlassian.jira.event.issue.AbstractIssueEventListener
import com.atlassian.jira.event.issue.IssueEvent
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.security.groups.GroupManager
import com.atlassian.jira.security.roles.ProjectRoleManager
import com.atlassian.mail.Email
import com.atlassian.mail.MailException
import com.atlassian.mail.MailFactory
import com.atlassian.mail.queue.SingleMailQueueItem
import com.nytimes.ep2.utils.Utils
import groovy.text.GStringTemplateEngine
import org.apache.log4j.Category

/*

This listener is fired at whatever workflow states you speciy in the Builtin Script Listener JIRA GUI.

*/
class NotifyApprovers extends AbstractIssueEventListener {

  Category log
  Utils u
  MutableIssue issue

  private void setup(MutableIssue issue) {
    this.issue = issue
    u = new Utils()
    log = u.getLog(NotifyApprovers.class.name)
  }
  
  @Override
  void workflowEvent(IssueEvent event) {
    
    setup(event.getIssue())

    log.debug "####################################################################"
    
    def issuetype = issue.getIssueTypeObject()
    if (issuetype.getName() == "Service Request")
      log.debug "Processing Service Request upon Update event."
    else 
      return
    
    Map approverTypes = [
      devApproval : [role : 'Development Approver', activity : 'Development Approval'],
      codeReview : [role : 'Code Reviewer', activity : 'Code Review'],
      qaApproval : [role : 'QA Approver', activity : 'QA Approval'],
      securityApproval : [role : 'Security Approver', activity : 'Security Approval'],
      stakeholderApproval : [role : 'Stakeholder Approver', activity : 'Stakeholder Approval']
    ]

    approverTypes.each { key, approverType ->  
      
      log.debug "approvertype: $approverType"

      def approver = u.getCustomField(issue, approverType.role)
      def activity = u.getCustomField(issue, approverType.activity)
      
      if (approvalNeeded(approver, activity)) {
	
	def params = [:]    
	params['toAddress'] = approver.emailAddress
	params['toDisplayName'] = approver.displayName
	params['role'] = approverType.role
	params['activity'] = approverType.activity
	params['issue'] = issue
	
	sendMail(params)
	
      }
    }

    log.debug "####################################################################"
  }
  
  private boolean approvalNeeded(User role, Object activityField) {
    
    return (role != null && activityField == null)
  }

  void sendMail(Map params) {

    ComponentManager componentManager = ComponentManager.getInstance()

    def watcherManager = componentManager.getWatcherManager()
    def customFieldManager = componentManager.getCustomFieldManager()
    def projectRoleManager = ComponentManager.getComponentInstanceOfType(ProjectRoleManager.class)
    def groupManager = ComponentManager.getComponentInstanceOfType(GroupManager.class)
    def userUtil = componentManager.getUserUtil()
    def mailServerManager = componentManager.getMailServerManager()
    def mailServer = mailServerManager.getDefaultSMTPMailServer()

    if (mailServer && ! MailFactory.isSendingDisabled()) {
      
      Writable template = mergeEmailTemplateBody(params)
      String body = template.toString()
      
      log.debug "body: $body"
      
      Email email = new Email(params.toAddress)
      email.setMimeType("text/plain")
      //email.setTo(params.toAddress)
      email.setFrom(mailServer.getDefaultFrom())
      email.setSubject(
	mergeEmailTemplateSubject(params).toString()
      )
      email.setBody(body)
      
      try {
	
	SingleMailQueueItem item = new SingleMailQueueItem(email);
	ManagerFactory.getMailQueue().addItem(item);
	
	log.info "Sent service request / approval request email. [to: $params.toAddress issue: $issue]"
	
      } catch (MailException e) {
	log.error ("Error sending email", e)
      }

    }

  }
  
  private Writable mergeEmailTemplateBody(Map params) {


    
    def template = '''\
Dear ${toDisplayName},

$issue.reporter.displayName created the service request $issue.
------------------------------------------------------------
As $role, you are being asked to approve or reject for the following activity: [$activity].

Please follow the link and approve or reject this issue.

Service Request Summary: $issue.summary
Key: $issue
URL: ${baseUrl}/browse/${issue} 
Components: $issue.componentObjects.name
Assignee: $issue.assignee.name
Reporter: $issue.reporter.name
Status: $issue.statusObject.name

${ issue.description == null ? "" : issue.description }

Thanks.
'''
    mergeEmailTemplate(params, template)
  }
  
  private Writable mergeEmailTemplateSubject(Map params) {
    def template = ""
    mergeEmailTemplate(params, template)
  }


  //
  // @see http://groovy.codehaus.org/api/groovy/text/GStringTemplateEngine.html
  //
  private Writable mergeEmailTemplate(Map params, String template) {
    
    GStringTemplateEngine engine = new GStringTemplateEngine()
    def binding = [:]

    MutableIssue issue
    issue = params.issue as MutableIssue
    
    ApplicationProperties applicationProperties = ComponentManager.getInstance().getApplicationProperties()

    binding.put("baseUrl", applicationProperties.getString(APKeys.JIRA_BASEURL))
    binding.putAll(params)

    log.debug "just before template creation..."
    log.debug "template: $template"
    log.debug "binding: $binding"

    def t = engine.createTemplate(template)
    def filledTemplate = t.make(binding)
    
    filledTemplate
  }

}
