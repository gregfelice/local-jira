package com.nytimes.ep2.utils

import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.issuetype.IssueType
import org.apache.log4j.Category

/*
  note, this will only work if we are in a transition/postfunction hook.
*/

Category log = Category.getInstance("com.nytimes.ep2.utils.UtilsTest")
log.setLevel(org.apache.log4j.Level.DEBUG)

def test
if (issue != null) {
  test = new UtilsTest(log, true, issue, transientVars)
} else {
  test = new UtilsTest(log, false, null, null)
}
return test.run()

///////////////////////////////////////////////////////////////////////////

class UtilsTest {

  Category log
  boolean withinTransition
  MutableIssue issue
  Map transientVars
  StringBuffer sb

  UtilsTest(Category log, boolean withinTransition, Issue issue, Map transientVars) {
    this.log = log
    this.withinTransition = withinTransition
    if (withinTransition) {
      this.issue = issue as MutableIssue
      this.transientVars = transientVars
    }
    this.sb = new StringBuffer()

    log.debug "withinTransition: $withinTransition"
  }

  String run() {

    // non-JIRA transition context dependent
    testLog()

    // JIRA transition context dependent
    if (withinTransition) {
      //testGetCurrentUser() // get me the current logged in user.
      //testGetUser() // get me a specific user by username.
      //testGetDevelopmentApprover() // get me a custom field value.
      //testCreateIssue() // create an issue.
      //testCreateSubTask() // create a subtask and attach to a parent.
      //testCreateSubTaskWithAssignee() // create a subtask, attach to parent, and assign subtask.
      //testCFS() // test neato dynamic custom field stuff
      //testGetIssueTypes()
      testWorkflowTransition()
    }
    sb.toString()
  }

  void testLog() {

    log.debug "START testLog()"
    Utils u = new Utils()
    def log = u.getLog("UtilsTest")
    assert log != null
    log.debug "testLog() testing the log debug output"
    log.debug "END testLog()"
  }

  void testGetCurrentUser() {

    log.debug "START testGetCurrentUser()"
    Utils u = new Utils()
    log.debug "transientVars: $transientVars"
    def username = u.getCurrentUser(transientVars).getName()
    log.debug u.getCurrentUser(transientVars).getName()
    assert username ==~ "greg.felice"
    log.debug "END testGetCurrentUser()"
  }

  void testGetUser() {

    log.debug "START testGetUser()"
    Utils u = new Utils()
    def user = u.getUser('greg.felice')
    log.debug "user : " + user.getName()
    log.debug "END testGetUser()"
  }

  void testGetDevelopmentApprover() {

    log.debug "START testGetDevelopmentApprover()"
    Utils u = new Utils()
    def devApprover = u.getCustomField(issue, "Development Approver")
    log.debug "devApprover: " + devApprover.getName()
    log.debug "END testGetDevelopmentApprover()"
  }

  void testCreateSubTaskWithAssignee() {

    log.debug "\n\nSTART testCreateSubTaskWithAssignee()"
    Utils u = new Utils()
    log.debug "issue: $issue"
    def project = issue.getProjectObject()
    def reporter = u.getUser('greg.felice')
    def assignee = u.getCustomField(issue, 'Development Approver')
    log.debug "development approver: $assignee"
    def params = [
      parentIssue: issue, // if omitted or null, will create a non-subtask
      issueType: '10', // review task - subtask example.
      project: project,
      assignee: assignee,
      reporter: reporter,
      summary: "testCreateSubTask - review task created by test"
    ]
    //log.debug "params: " + params
    def issue = u.createIssue(params)
    log.debug "created issue: " + issue
    log.debug "END testCreateSubTaskWithAssignee()"
  }

  void testCreateSubTask() {

    log.debug "\n\nSTART testCreateSubTask()"
    Utils u = new Utils()
    def project = issue.getProjectObject()
    def assignee = u.getUser('greg.felice')
    def reporter = u.getUser('greg.felice')
    def params = [
      parentIssue: issue, // if omitted or null, will create a non-subtask
      issueType: '10', // review task - subtask example.
      project: project,
      assignee: assignee,
      reporter: reporter,
      summary: "testCreateSubTask - review task created by test"
    ]
    log.debug "params: " + params
    def issue = u.createIssue(params)
    log.debug "created issue: " + issue
    log.debug "END testCreateSubTask()"
  }

  /*
    to test: create a service request in ballast project
   */

  void testCreateIssue() {

    log.debug "\n\nSTART testCreateIssue()"
    Utils u = new Utils()
    def project = issue.getProjectObject()
    def assignee = u.getUser('greg.felice')
    def reporter = u.getUser('greg.felice')
    def params = [
      parentIssue: null, // if omitted or null, will create a non-subtask
      issueType: '7', // story
      project: project,
      assignee: assignee,
      reporter: reporter,
      summary: "testCreateIssue - story created by test"
    ]
    log.debug "params: " + params
    def issue = u.createIssue(params)
    log.debug "created issue: " + issue
    log.debug "END testCreateIssue()"
  }

  // test issue creation with a customfield.
  void testCFS() {

    log.debug "testCFS"
    Utils u = new Utils()
    def user = u.getUser('automation')
    def project = issue.getProjectObject()
    def assignee = u.getUser('greg.felice')
    def reporter = u.getUser('greg.felice')
    def developmentApprover = u.getUser('gsebel')
    def params = [
      parentIssue: null,   // if omitted or null, will create a non-subtask
      issueType: '7',      // story
      project: project,
      assignee: assignee,
      reporter: reporter,
      summary: "testCFS - story created by test",
      developmentApprover: developmentApprover
    ]
    def newIssue = u.createIssue(params)
    log.debug "newIssue: $newIssue"
  }

  // currently failing
  void testGetIssueTypes() {
    log.debug "START testGetIssueTypes"
    Utils u = new Utils()
    IssueType issueType
    def issueTypes = u.getIssueTypes()
    issueTypes.each {i ->
      issueType = i
      log.debug "issuetype: $issueType"
      log.debug "issuetype id: $issueType.getId()"
      //log.debug "issuetype: $issueType.getDescription()"
    }
    log.debug "issueTypes: $issueTypes"
    log.debug "END testGetIssueTypes"
  }

  void testWorkflowTransition() {
    Utils u = new Utils()
    log.debug "START testWorkflowTransition()"
    def actionId = "4" // Start Progress
    Map params = [:]
    params['transientVars'] = transientVars
    params['user'] = u.getCurrentUser(transientVars)
    log.debug "params/transient vars in test: $params"
    u.transitionIssue(issue, actionId, params)
    log.debug "END testWorkflowTransition()"
  }

}

