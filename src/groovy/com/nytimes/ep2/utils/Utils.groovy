package com.nytimes.ep2.utils

import com.atlassian.crowd.embedded.api.User
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.bc.issue.IssueService
import com.atlassian.jira.bc.issue.IssueService.CreateValidationResult
import com.atlassian.jira.bc.issue.IssueService.IssueResult
import com.atlassian.jira.bc.issue.IssueService.TransitionValidationResult
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.status.Status
import com.atlassian.jira.user.util.UserUtil
import com.atlassian.jira.util.ErrorCollection
import com.atlassian.jira.util.ImportUtils
import com.opensymphony.workflow.WorkflowContext
import com.opensymphony.workflow.spi.WorkflowEntry
import groovy.inspect.Inspector
import org.apache.log4j.Category
import org.ofbiz.core.entity.DelegatorInterface
import org.ofbiz.core.entity.GenericValue
import com.atlassian.jira.issue.*

/*
  
  Reference
  ----
  ** issue operations https://developer.atlassian.com/display/JIRADEV/Performing+Issue+Operations

  ** subtasks http://docs.atlassian.com/jira/latest/com/atlassian/jira/config/SubTaskManager.html

  (deprecated < 4.0) https://developer.atlassian.com/display/JIRADEV/Creating+and+Editing+an+Issue

  https://developer.atlassian.com/display/JIRADEV/Retrieving+issue%27s+links
  https://developer.atlassian.com/display/JIRADEV/Working+with+Custom+Fields

*/
class Utils {

  private Category log
    
  public Utils() {
    log = Category.getInstance("com.nytimes.ep2.utils.Utils")
    log.setLevel(org.apache.log4j.Level.DEBUG) 
  }
  
  void inspect(Object o) {
    log.debug "%%%%%%%%%%INSPECT%%%%%%%%%%%"
    Inspector i = new Inspector(o)
    log.debug "methods: " + i.getMethods()
    log.debug "metamethods: " + i.getMetaMethods()
    log.debug "public fields: " + i.getPublicFields()
    log.debug "object: " + i.getObject()
    log.debug "%%%%%%%%%%INSPECT%%%%%%%%%%%"
  }

  String test() {
    return "Utils.test() return value"
  }
  
  Category getLog(String className) {
    def Category log = Category.getInstance(className)
    log.setLevel(org.apache.log4j.Level.DEBUG)
    log
  }
  
  User getCurrentUser(Map transientVars) {
    
    ComponentManager componentManager = ComponentManager.getInstance()
    UserUtil userUtil = componentManager.getUserUtil()
    String currentUser
    if (transientVars) {
      currentUser = ((WorkflowContext) transientVars.get("context")).getCaller()
    } else {
      currentUser = componentManager.getJiraAuthenticationContext().getUser().getName()
    }
    User currentUserObj = userUtil.getUser(currentUser)
    currentUserObj
  }

  User getUser(String username) {
    def userUtil = ComponentManager.getInstance().getUserUtil()
    def user = userUtil.getUser(username)
    user
  }
  
  Object getCustomFieldValue(Issue issue, String fieldName) {
    
    getCustomField(issue, fieldName)
    return (customField == null) ? null : customField.getValue(issue)
  }

  Object getCustomField(Issue issue, String fieldName) {
    
    CustomFieldManager customFieldManager = ComponentManager.getInstance().getFieldManager().getCustomFieldManager()
    CustomField customField = customFieldManager.getCustomFieldObjectByName(fieldName)
    log.debug "fieldname: $fieldName"
    log.debug "customfield: " + customField.getValue(issue)
    return (customField == null) ? null : customField.getValue(issue)
  }

  // another way of getting a customfield.
  Object getCustomFieldB(Issue issue, String fieldName) {
    
    CustomFieldManager customFieldManager = ComponentManager.getInstance().getFieldManager().getCustomFieldManager()
    CustomField customField = customFieldManager.getCustomFieldObjectByName(fieldName)
    log.debug "fieldname: $fieldName"
    def cfv = issue.getCustomFieldValue(customField)
    log.debug "customfield: " + cfv
    return (customField == null) ? null : cfv
  }
  
  // safety method tocentralize obtaining usernames against strange objects.
  String getUserName(Object user) {
    
    if (user.getClass() == com.atlassian.crowd.embedded.ofbiz.OfBizUser) {
      return user.name
    } else {
      log.error "#######ERROR###########"
      log.error "getUserName: object user is not of a known type."
      log.error "user class: $user.getClass()"
      log.error "#######ERROR###########"
      return null
    }
  }
  
  List getSubtasksOfType(Issue issue, String subtaskType) {
    
    def subtasks = ComponentManager.getInstance().getSubTaskManager().getSubTaskObjects(issue)
    def taskSet = []
    subtasks.each {subtask ->
      if (subtask.getIssueTypeObject().getName() == subtaskType) {
	taskSet.add(subtask)
      }
    }
    return taskSet
  }

  void createTask() {
  }

  void createSubtask(Issue issue, Issue newIssue) {
  
    def subTaskManager = ComponenentManager.getInstance().getSubTaskManager()
    subTaskManager.createSubTaskIssueLink(issue, newIssue, getUser(params))
    // reindex again after creating subtask link
    // reindexIssue(newIssue.genericValue)
  }
  
  
  //
  // http://docs.atlassian.com/jira/latest/com/atlassian/jira/issue/IssueInputParameters.html
  //
  Issue createIssue(Map params) {

    Closure getName = { cf -> return cf.name } 
    
    Map cfMap = [
      developmentApprover : [ fieldName : "Development Approver", strategy : getName ]
    ]
    
    log.debug "********START****************"
    def user = getUser('automation')
    def isSubTask
    if (params.parentIssue)
      isSubTask = true
    else
      isSubTask = false
    IssueService issueService = ComponentManager.getInstance().getIssueService()
    IssueInputParameters inputParams = issueService.newIssueInputParameters()

    inputParams.setProjectId(params.project.getId())
    .setIssueTypeId(params.issueType)
    .setSummary(params.summary)
    .setReporterId(params.reporter.getName())
    .setAssigneeId(params.assignee.getName())

    CustomFieldManager customFieldManager = ComponentManager.getInstance().getFieldManager().getCustomFieldManager()
    
    params.each { key, value -> 
      log.debug "key: $key / value: $value"
      if (cfMap[key]) { // custom field found
	def cfName = cfMap[key].fieldName
	def cfStrategy = cfMap[key].strategy
	log.debug "Custom field found: " + cfName
	log.debug "Custom field strategy: " + cfStrategy
	CustomField customField = customFieldManager.getCustomFieldObjectByName(cfName)
	def cfId = customField.getIdAsLong()
	def cfValue = cfStrategy(value)
	log.debug "strategy output: " + cfValue 
	inputParams.addCustomFieldValue(cfId, cfValue)
      }
    }
    
    CreateValidationResult createValidationResult
    if (isSubTask) {
      createValidationResult = issueService.validateSubTaskCreate(user, params.parentIssue.getId(), inputParams)
    } else {
      createValidationResult = issueService.validateCreate(user, inputParams)
    }
      
    if (createValidationResult.isValid()) {
      IssueResult createResult = issueService.create(user, createValidationResult)
      if (!createResult.isValid()) {
	log.error "createResult invalid."
	return null
      }
      Issue newIssue = createResult.getIssue()
      if (isSubTask) {
	def stmgr = ComponentManager.getInstance().getSubTaskManager()
	stmgr.createSubTaskIssueLink(params.parentIssue, newIssue, user)
      }
      log.debug "*********END***************"
      return newIssue
    } else {
      log.error "create validation result not valid: " + createValidationResult.getErrorCollection() 
      return  null
    }
  }
  
  public List getIssueTypes() {
    //ConstantsManager constantsManager = ComponentManager.getInstance().getConstantsManager()
    //return constantsManager.getAllIssueTypeObjects().asList()
  }

  public void transitionIssue(MutableIssue issue, String actionId, Map params) {
    log.debug "START testWorkflowTransition()"
    Thread executorThread = new Thread (
      new Runnable() {
        void run() {
	  log.debug "in thread, sleeping 100 msec..." + System.currentTimeMillis()
          Thread.sleep(100)
	  log.debug "in thread, waking..." + System.currentTimeMillis()
          doTransition(issue, actionId, params)
	  log.debug "in thread, transition done..." + System.currentTimeMillis()
        }
      })
    executorThread.start()
    log.debug "END testWorkflowTransition()"
  }

  /**

current bug: https://answers.atlassian.com/questions/27352/issueservice-transitions-the-issue-but-its-status-stays-the-same

this is an issue. we have workflow transitions getting 'stuck', and not reverting to the correct step. not sure why. 

can we use a listener to force the issues back to the state we desire? 
can we devise safe code for a in transition transition?
can we cancel the current transition after presenting the transition screen, and running the post function?


   */
  private void doTransition(MutableIssue issue, String actionId, Map params) {
    
    log.debug "in doTransition. new thread. ;)"
    
    ComponentManager componentManager = ComponentManager.getInstance()
    
    DelegatorInterface gd = (DelegatorInterface) componentManager.getComponentInstanceOfType(DelegatorInterface.class)
    GenericValue gv = gd.findByPrimaryKey("OSWorkflowEntry", ["id": issue.getWorkflowId()])
    if (gv.get("state") == WorkflowEntry.CREATED) {
      gv.set("state", WorkflowEntry.ACTIVATED)
      gv.store()
    }
    
    // reload issue
    issue = ComponentManager.getInstance().getIssueManager().getIssueObject(issue.id)
    log.debug "doTransition: reloaded issue: $issue"
    
    // when running in a different thread we lose the authcontext - causes weird and wonderful errors,
    // hence we set it explicitly
    // log.debug "transientvars: $params.transientVars"
    User user = params.user
    // log.debug "doTransition: current user $user.name"
    
    ComponentManager.getInstance().getJiraAuthenticationContext().setLoggedInUser(user)
    
    actionIssue(null, issue, actionId as Integer, user)
    
    boolean wasIndexing = ImportUtils.isIndexIssues();
    
    // reload issue
    issue = componentManager.getIssueManager().getIssueObject(issue.id)
    componentManager.getIndexManager().reIndex(issue)
    ImportUtils.setIndexIssues(wasIndexing)
  }

  private void actionIssue(String additionalScript, MutableIssue theIssue, Integer actionId, User user) {
    
    log.debug "in actionIssue. issue: $theIssue actionId: $actionId user: $user"
    
    IssueService issueService = ComponentManager.getComponentInstanceOfType(IssueService.class)
    IssueInputParameters issueInputParameters = new IssueInputParametersImpl([:])
    
    def scriptParams = [:]
    scriptParams.put("issueInputParameters", issueInputParameters)
    
    // commented out for now
    // ConditionUtils.doAdditional(additionalScript, theIssue, scriptParams)
    
    issueInputParameters = scriptParams['issueInputParameters'] as IssueInputParameters
    
    TransitionValidationResult validationResult = issueService.validateTransition(user, theIssue.id, actionId as Integer, issueInputParameters)
    
    ErrorCollection errorCollection = validationResult.errorCollection
    if (errorCollection.hasAnyErrors()) {
      // We used to carry on because sometimes these seem spurious. More investigation needed.
      log.error(errorCollection)
      log.error("Not attmpting transition")
      return
    }
    
    errorCollection = issueService.transition(user, validationResult).errorCollection
    if (errorCollection.hasAnyErrors()) {
      log.error(errorCollection)
    }
    log.info("Transitioned issue $theIssue through action \"$actionId\"")
    
    // reload issue
    theIssue = ComponentManager.getInstance().getIssueManager().getIssueObject(theIssue.id)
    Status finalStatus = theIssue.statusObject
    log.debug "Final status: ${finalStatus.getName()} (${finalStatus.id})"
  }

}

